{
    'name': 'Project 2 Update',
    'version': '11.1.0',
    'author': 'PT Arkana',
    'license': 'OPL-1',
    'category': 'Tailor-Made',
    'website': 'http: //www.arkana.co.id/',
    'summary': 'Custom-built Odoo',
    'description': '''
    ''',
    'depends': [
        'project', # python
        # 'sale', # python
    ],
    'data': [
        'views/task_doc.xml',
        'views/task_doc_line.xml',
        'views/doc_type.xml',
        'views/modify_project.xml',
        'views/task_assignment_line.xml',

    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}