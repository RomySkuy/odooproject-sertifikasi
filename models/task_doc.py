# TaskDoc
# --- field.Many2one('Document Type
# --- field.Binary('Pdf')
# - Name
# - Upload Time Datetime
# - Upload By	

from odoo import models, fields, api

class TaskDoc(models.Model):
    _name = 'task.doc'

    doc_type_id = fields.Many2one('doc.type', 'Document Type')
    upload_doc = fields.Binary('Pdf')
    name = fields.Char('Name')
    upload_time = fields.Datetime('Upload Time')
    upload_by = fields.Many2one('res.users', 'Upload By')

    document_id = fields.Many2one('project.task', 'Doc')

    @api.onchange('upload_doc')
    def _onchange_upload_doc(self):
        # set auto-changing field
        if self.upload_doc:
            self.upload_time = fields.Datetime.now()
    

class ProjectTask(models.Model):
    _inherit = 'project.task'

    document_ids = fields.One2many('task.doc', 'document_id', 'Document')

