# TaskDocLine
# --- fields.Many2one('project.task.document')
# --- fields.Boolean( Valid)
# --- fields.Boolean( Skip)
# --- fields.Many2one('Document Type')
# ----------------------------------------



from odoo import models, fields
from odoo.exceptions import UserError, ValidationError 

class TaskDocLine(models.Model):
    _name = 'task.doc.line'
    _rec_name = 'doc_task_id'

    doc_task_id = fields.Many2one('task.doc', 'Nama Document')
    valid_bool = fields.Boolean(string='Valid')
    skip_bool = fields.Boolean(string='Skip')
    doc_type_id = fields.Many2one('doc.type', 'Document Type')

    task_id = fields.Many2one('project.task', 'Task')

    
class ProjectTask(models.Model):
    _inherit = 'project.task'

    document_line_ids = fields.One2many('task.doc.line', 'task_id', 'Document')
    
    stage_id_name = fields.Char(related='stage_id.name', string='Stage Name')

    

    def action_process(self):

        # Validasi document stage saat ini otomatis
        # Cari doctype di stage saat ini
        cur_doc_types = self.stage_id.stages_doc_ids # Akta, SIUP, NPWP


        # self.browse([3, 7, 9, 4, 10,])

        # self.env['kampus.kuliah'].search([]).mapped('umur')

        all_valid = True
        for cur_doc_type in cur_doc_types:
            for line in self.document_line_ids:
                if (line.doc_type_id == cur_doc_type and not (line.valid_bool or line.skip_bool)):                    
                    all_valid = False
                    raise UserError('Tidak Valid ' + line.doc_type_id.name)
                        
        # # End Time current stage
        # for line in self.assignment_line_ids:
        #     if (line.stage_id.id == self.stage_id.id):
        #         line.end_time = now()

        # Cari stage berikutnya
        next_sequence = self.stage_id.sequence+1
        next_stage = self.env['project.task.type'].search([('sequence', '=', next_sequence)], limit=1)
        # Ubah stage
        self.stage_id = next_stage.id
        
        # Tambahkan document dari stage selanjutnya otomatis
        for next_doc_type in next_stage.stages_doc_ids: #Invoice, Kwitansi
            # self.document_line_ids = ({ 
            #     'doc_type' : [(0,0, next_doc_type.id)],
            #     'valid' : False,
            #     'skip' : False
            # })

            sei8lf.document_line_ids = [
                (0, 0, {
                    'doc_type_id' : next_doc_type.id,
                    'valid': False,
                    'skip': False
                })
            ]

            # self.env['task.doc.line'].create({
            #     'doc_type_id' : next_doc_type.id,
            #     'valid_bool' : False,
            #     'skip_bool' : False,
            #     'task_id' : self.id
            # })
    

        # # Start Time next stage
        # for line in self.assignment_line_ids:
        #     if (line.stage_id.id == next_stage.id):
        #         line.start_time = now()
        

