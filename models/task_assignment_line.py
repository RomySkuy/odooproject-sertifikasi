import datetime
import math
from odoo import models, fields, api
class TaskAssignmentLine(models.Model):
    _name = 'task.assignment.line'
    
    tasktype_id = fields.Many2one('project.task.type', 'Task')
    user_id = fields.Many2one('res.users', 'Name')
    kpi = fields.Float('kpi (Hour)')
    start_time = fields.Datetime('Start Time')
    end_time = fields.Datetime('End Time')
    duration = fields.Float(string='Duration', compute='_compute_duration')
    duration_2 = fields.Float(string='Duration', compute='_compute_duration2')
    efisien = fields.Float(string='Efficiency', compute='_compute_efficency')
    # efisien = fields.Float(string='Efficiency',)
    task_id = fields.Many2one('project.task', 'Task')

      # menghitung durasi
    @api.multi
    def _compute_duration(self):
        for doc in self:
            qqq = 3
            time_diff = doc.end_time - doc.start_time
            year = time_diff.days//(365.25)
            month = (time_diff.days-year*365.25)//(365.25/12)
            day = ((time_diff.days-year*365.25) - month*(365.25/12))
            hours_diff = math.floor(round(float(time_diff.days) * 24 + (float(time_diff.seconds) / 3600)))
            doc.duration = hours_diff

    @api.multi
    def _compute_duration2(self):
        for doc in self:
            qqq = 3
            z1 = datetime.datetime.strptime('Mar 12 2019  5:00PM', '%b %d %Y %I:%M%p')
            y1 = z1-( doc.start_time + datetime.timedelta(hours=7) )
            x1 = math.floor(float(y1.seconds)/3600)
            
            z2 = datetime.datetime.strptime('Mar 12 2019  8:00AM', '%b %d %Y %I:%M%p')
            y2 = (doc.end_time + datetime.timedelta(hours=7))- z2
            x2 = math.floor(float(y2.seconds)/3600)

            d1 = doc.end_time - doc.start_time 
            # Alt 1
            # d2 = (d1.days - 1) * 8
            # if d2<0:
            #     d2 = 0
            # Alt 2
            if (d1.days -1 ) > 0: # Kalau disuruh hari ini , kelar besok
                d2 = (d1.days - 1) * 8
                result = x1 + x2 + d2
            else:   # Kalau disuruh hari ini ,beres hari ini
                d2 = 0
                result = math.floor(d1.seconds / 3600)
            doc.duration_2 = result

    @api.multi
    def _compute_efficency(self):
        for line in self:
            eff_diff = line.kpi / line.duration_2 *100
            line.efisien = eff_diff
            
class HrEmployeeEmail(models.Model):
    _inherit = 'project.task'

    @api.multi
    def action_send_mail(self):
        """Dijalankan, jika user menekan tombol 'Send Email'"""
        template = self.env.ref('project_karyawan.template_mail_test_employee')
        compose_form = self.env.ref('mail.email_compose_message_wizard_form')
        ctx = dict(
            default_model='project.task',
            default_res_id=self.id,
            default_use_template=bool(template),
            default_template_id=template.id,
            default_composition_mode='comment',
            custom_layout="mail.mail_notification_light",
        )
        return {
            'name': 'Compose Email',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }

class ProjectTask(models.Model):
    _inherit = 'project.task'

    line_ids = fields.One2many('task.assignment.line', 'task_id', 'Assignment')

  