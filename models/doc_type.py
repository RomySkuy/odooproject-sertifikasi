from odoo import models, fields

class DocType(models.Model):
    _name = 'doc.type'

    name = fields.Char(string='Jenis Dokumen')

class ProjectTaskType(models.Model):
    _inherit = 'project.task.type'

    stages_doc_ids = fields.Many2many(
        comodel_name='doc.type',
        string='Nama Dockumen Stages',
        )

